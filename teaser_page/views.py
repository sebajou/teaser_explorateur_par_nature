from django.shortcuts import render
from django.template.loader import get_template
from django.http import HttpResponse

# Create your views here.
def teaser_page(request):
    """Main page"""
    template = get_template('teaser_page/teaser_page.html')
    return HttpResponse(template.render(request=request))
