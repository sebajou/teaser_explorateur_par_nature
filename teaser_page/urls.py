from django.urls import path

from . import views

urlpatterns = [
    path('', views.teaser_page, name='teaser_page'),
]
